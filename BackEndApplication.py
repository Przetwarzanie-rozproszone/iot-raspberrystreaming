import sys

from azure.iot.hub import IoTHubRegistryManager
from azure.iot.hub.models import CloudToDeviceMethod, CloudToDeviceMethodResult

from builtins import input

CONNECTION_STRING = "HostName=cdv-iothub.azure-devices.net;SharedAccessKeyName=service;SharedAccessKey=JgN46S02hzjYOF1XuTNmZO2/sSsOEfM/pwpm7UY8C8o="
DEVICE_ID = "raspberryID"

METHOD_NAME = None
METHOD_PAYLOAD = None

def iothub_devicemethod_sample_run():
    try:
        registry_manager = IoTHubRegistryManager(CONNECTION_STRING)
        while True:
            METHOD_NAME = input("Method name: ")
            METHOD_PAYLOAD = input("Method payload: ")

            deviceMethod = CloudToDeviceMethod(method_name=METHOD_NAME, payload=METHOD_PAYLOAD)
            response = registry_manager.invoke_device_method(DEVICE_ID, deviceMethod)

            print ( "" )
            print ( "Device Method called" )
            print ( "Device Method name       : {0}".format(METHOD_NAME) )
            print ( "Device Method payload    : {0}".format(METHOD_PAYLOAD) )
            print ( "" )
            print ( "Response status          : {0}".format(response.status) )
            print ( "Response payload         : {0}".format(response.payload) )

            input("Press Enter to continue...\n")

    except Exception as ex:
        print ( "" )
        print ( "Unexpected error {0}".format(ex) )
        return
    except KeyboardInterrupt:
        print ( "" )
        print ( "IoTHubDeviceMethod sample stopped" )

if __name__ == '__main__':
    print ( "    Connection string = {0}".format(CONNECTION_STRING) )
    print ( "    Device ID         = {0}".format(DEVICE_ID) )

    iothub_devicemethod_sample_run()